use <threadlib/threadlib.scad>  // https://github.com/adrianschlatter/threadlib?tab=readme-ov-file#installation

$fn = 100;
epsilon = 0.01; // to unambiguate face clippings

// abbreviations: r := radius and h := height

module stand(outer_r = 25, inner_r, h)
{
    color("gray", 0.6) difference()
    {
        hull()
        {
            cylinder(h = h, r = outer_r);
            translate([ -2.5 * outer_r, 0, 0 ]) cylinder(h = h, r = outer_r * 0.7);
        }
        translate([ 0, 0, -epsilon ]) cylinder(h = h + 2 * epsilon, r = inner_r);
    }
}

module scope(h = 200, r)
{
    color("red", 0.3) cylinder(h = h, r = r, center = true);
}

module sleeve(thread_spec, bot, inner_r, screw_h, thread_pitch)
{
    difference()
    {
        union()
        {
            translate([ 0, 0, -bot[0] ]) cylinder(h = bot[0], r = bot[1]);
            translate([ 0, 0, thread_pitch / 2 - epsilon ])
                bolt(thread_spec, turns = (screw_h / thread_pitch) - 1, higbee_arc = 30);
        };
        translate([ 0, 0, -(bot[0] + epsilon) ]) cylinder(h = 100, r = inner_r);
        // M3 insert holes
        translate([ 0, 0, screw_h - 15 ]) for (n = [0:2])
        {
            rotate([ 0, 0, 120 * n ])
            {
                r = 4;  // see https://cnckitchen.store/cdn/shop/files/7_59c341df-c335-4ea2-8792-56776fbc6194.png?v=1686081921
                rotate([ 0, 90, 0 ]) cylinder(h = 40, r = r / 2);
                translate([ inner_r + 4, 0, 0 ]) rotate([ 0, 90, 0 ])
                    cylinder(h = 3, r1 = r / 2 - epsilon, r2 = r);
            }
        }
        // cut thread outer diameter to M35
        translate([ 0, 0, epsilon ]) difference()
        {
            h = screw_h;
            r = 35;
            cylinder(h = h, r = (r + 5) / 2);
            cylinder(h = h, r = r / 2);
        }
        step_h = 5;
        translate([ 0, 0, -step_h ]) cylinder(h = screw_h + step_h + epsilon, r = 13);
    };
}

module sleeve_nut(thread_spec, turns, d)
{
    color("gray", 0.6) translate([ 0, 0, 15 ]) nut(thread_spec, turns = turns, Douter = d, higbee_arc = 30);
}

// implicit unit: mm
stand_h = 10;
stand_inner_r = 17;
scope_r = 12;
sleeve_bot_h = 10;
sleeve_screw_h = 50;
thread_pitch = 4;
thread_spec = str("M36x", thread_pitch);
nut_h = 20;
nut_turns = (nut_h / thread_pitch) - 1;
nut_d = 47;

sleeve_nut(thread_spec = thread_spec, d = nut_d, turns = nut_turns);
sleeve(thread_spec = thread_spec, bot = [ sleeve_bot_h, nut_d / 2 ], inner_r = scope_r, screw_h = sleeve_screw_h,
       thread_pitch = thread_pitch);
stand(h = stand_h, inner_r = stand_inner_r);
scope(r = scope_r);
